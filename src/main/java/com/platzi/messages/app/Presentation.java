/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.platzi.messages.app;

import com.platzi.messages.app.model.Mensaje;
import com.platzi.messages.app.service.MensajeService;
import java.sql.Connection;
import java.util.Scanner;

/**
 *
 * @author USUARIO
 */
public class Presentation {
    public static void main(String[] args) {

        // glpat-zv44t7ZJ_Qv3R6UqjP8C
        // platzi-mensajeapp
        
        Scanner sc = new Scanner(System.in);
        int opcion=0;
        do{
            System.out.println("====================");
            System.out.println("Aplicacion de mensajes");
            System.out.println("1. crear mensaje");
            System.out.println("2. listar mensaje");
            System.out.println("3. editar mensaje");
            System.out.println("4. eliminar mensaje");
            System.out.println("5. salir");
            
            opcion = sc.nextInt();
            switch(opcion){
                case 1: MensajeService.crearMensaje();break;
                case 2:MensajeService.listarMensaje();break;
                case 3:MensajeService.actualizarMensaje(new Mensaje()); break;
                case 4:MensajeService.borrarMensaje(opcion);break;
                case 5: break;
                default:break;
            }
        }while(opcion!=5);
        Conexion con = new Conexion();
        try (Connection cnx = con.getConnection()){
            
        }catch(Exception e){
            System.out.println("Error "+e);
        }
    }
}
