/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.platzi.messages.app.model;

/**
 *
 * @author USUARIO
 */
public class Mensaje {
    
    int idMensaje;
    String mensaje;
    String autor;
    String fecha;

    public Mensaje(int idMensaje, String mensaje, String autor, String fecha) {
        this.idMensaje = idMensaje;
        this.mensaje = mensaje;
        this.autor = autor;
        this.fecha = fecha;
    }

    public Mensaje() {
    }

    
    public int getIdMensaje() {
        return idMensaje;
    }

    public void setIdMensaje(int idMensaje) {
        this.idMensaje = idMensaje;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public String getAutor() {
        return autor;
    }

    public void setAutor(String autor) {
        this.autor = autor;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }
    
    
}
