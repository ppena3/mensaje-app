/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.platzi.messages.app;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 *
 * @author USUARIO
 */
public class Conexion {
    private Connection con;
    
    public Connection getConnection(){
        if (con ==null){
            try{
            con = DriverManager.getConnection("jdbc:mysql://localhost:3306/mensaje-app","root","");
            
            }catch(SQLException e ){
                System.out.println("Error "+e);
            }
        }
    
        return con;
    }
}
